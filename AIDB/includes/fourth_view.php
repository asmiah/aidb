<div class="navbar navbar-default navbar-fixed-top scroll-me">
        <!-- pass scroll-me class above a tags to starts scrolling -->
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <!--<img src="assets/img/logo.png"  alt=""/>-->
                </a>
            </div>
            <div class="navbar-collapse collapse animated bounceInDown">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php#works">GO BACK TO PORTFOLIO</a></li>
                </ul>
            </div>

        </div>
    </div>
    <!-- NAVBAR END  -->
    <!--HEADER SECTION END  -->
    <section id="services">
    <div class="container">
            <div class="row text-center">
                <div class="col-md-6 col-md-offset-3 pad-bottom" data-scroll-reveal="enter from the bottom after 0.2s">
                    <h2><strong>PORTFOLIO | SMARTAGENT</strong></h2>
                </div>
            </div>
    <div class="row">
        <div class="col-md-4" data-scroll-reveal="enter from the bottom after 0.4s">
            <h4><strong>SMARTAGENT</strong></h4>
                <p>This website is under construction.
                This website is being designed and developed as part
                of my final year project.</p>

                <p>Smart Agent is a Web based tenant management system which alerts estage
                agent admin users of tenants who have missed payments, and or made late payments. Smart Agent
                allows you to check a tenants payment history as well as amend, update or delete. Smart Agent
                is powered by two very popular frameworks, such as CodeIgniter as a back end framework and 
                bootstrap as the front end framework.</p>
        </div> <!-- /col-md-4 -->
        
        <div class="col-md-8" data-scroll-reveal="enter from the bottom after 0.4s">
            <!-- begin macbook pro mockup -->
            <div class="md-macbook-pro md-glare">
                <div class="md-lid">
                    <div class="md-camera"></div>
                    <div class="md-screen">
                    <!-- content goes here -->                
                        <div class="tab-featured-image">
                            <div class="tab-content">
                                <div class="tab-pane  in active" id="tab1">
                                    <img src="assets/img/portfolio/smartAgentWeb.png">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="md-base"></div>
            </div> <!-- end macbook pro mockup -->

            <span class='fancyZoomLinks'><strong><a target='_blank' href='../PP'>Click here to view site</a></strong></span>

        </div> <!-- / .col-md-8 -->

    </div> <!--/ .row -->
</div> <!-- end sidetab container -->
<div class="container">
    <div class="row text-center">
        <div class="col-md-6 col-md-offset-3 pad-bottom" data-scroll-reveal="enter from the bottom after 0.2s">
            <h4><strong>TECHNOLOGIES USED</strong></h4>
        </div>
    </div>
    <div class="row" data-scroll-reveal="enter from the bottom after 0.6s">                   
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width: 5%;">
                <span class="sr-only">5% Complete</span>
            </div>
            <span class="progress-type">HTML / HTML5</span>
            <span class="progress-completed">5%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                <span class="sr-only">100% Complete (danger)</span>
            </div>
            <span class="progress-type">CSS / CSS3 / FRONT END FRAMEWORKS</span>
            <span class="progress-completed">100%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                <span class="sr-only">100% Complete (success)</span>
            </div>
            <span class="progress-type">PHP | FRAMEWORK | CODEIGNITER</span>
            <span class="progress-completed">100%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                <span class="sr-only">100% Complete (info)</span>
            </div>
            <span class="progress-type">MVC</span>
            <span class="progress-completed">100%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%">
                <span class="sr-only">25% Complete (success)</span>
            </div>
            <span class="progress-type">Json</span>
            <span class="progress-completed">25%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                <span class="sr-only">15% Complete (warning)</span>
            </div>
            <span class="progress-type">JavaScript / jQuery</span>
            <span class="progress-completed">15%</span>
        </div>
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
                <span class="sr-only">20% Complete</span>
            </div>
            <span class="progress-type">Ajax</span>
            <span class="progress-completed">20%</span>
        </div>
    </div>
</div>

</section>