<div class="navbar navbar-default navbar-fixed-top scroll-me">
        <!-- pass scroll-me class above a tags to starts scrolling -->
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <!--<img src="assets/img/logo.png"  alt=""/>-->
                </a>
            </div>
            <div class="navbar-collapse collapse animated bounceInDown">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#header">HOME</a></li>
                    <li><a href="#services">SERVICES</a></li>
                    <li><a href="#works">WORK</a></li>
                    <li><a href="#about">ABOUT</a></li>
                    <li><a href="#contact">CONTACT</a></li>
                </ul>
            </div>

        </div>
    </div>
    <!-- NAVBAR END  -->
    <div id="header">
        <div class="overlay">
            <div class="container">
                <div class="row scroll-me">
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 text-center ">

                        <h1 class="mainHeader animated bounceInDown">Abdush Samad Miah
                        </h1>
                        
                        <span class="developer animated bounceInLeft"><i class="ion-network"></i> Developer </span> 
                        <span class="graphicDesigner animated lightSpeedIn"><i class="ion-paintbrush"></i> Graphic Designer </span> 
                        <span class="technician animated bounceInRight"><i class="ion-gear-b"></i> Technician</span>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <a data-scroll-reveal="enter from the bottom after 0.6s" href="#about" class="btn-xlarge btn-default set-bk-clr animated bounceInUp">FIND OUT MORE</a>                    
                        <br />
                        <br />
                        <br />
                        <br />
                            <a target="_blank" href="https://uk.linkedin.com/pub/abdush-samad-miah/a1/8b3/862"><img class="linkedIn animated rotateIn" src="assets/img/social/linkedIn.png" alt="li"/></a>
                            <a target="_blank" href="https://bitbucket.org/asmiah"><img class="bb animated rotateIn" src="assets/img/social/bb.png" alt="bb"/></a>
                            <a target="_blank" href="https://github.com/asmiah"><img class="git animated rotateIn" src="assets/img/social/git.png" alt="git"/></a>
                    </div>
                </div>
                
            </div>
        </div>

    </div>
    <!--HEADER SECTION END  -->
    <section id="services">
        <div class="container">
            
            <div class="row text-center">
                <div class="col-md-6 col-md-offset-3 pad-bottom" data-scroll-reveal="enter from the bottom after 0.2s">
                    <h2><strong>SERVICES</strong></h2>
                </div>
            </div>
            
            <div class="row text-center pad-bottom" data-scroll-reveal="enter from the bottom after 0.4s">
                
                <div class="col-md-4">
                    <img src="assets/img/services/webdevelopment.png" alt="webdevelopment"/>
                    <h4><strong>Web &amp; Mobile</strong></h4>
                    Creating stunning &amp; intuitive interfaces 
                    is what I'm all about. I thoroughly enjoy developing
                    new and exciting websites, and do so with a passion.
                    I have the ability to use a range of different languages, 
                    and utilies new and improved approaches, to provide efficiency 
                    and maximised security.
                </div>
                
                <div class="col-md-4">
                    <img src="assets/img/services/graphicDesign.png" alt="graphic design"/>
                    <h4><strong>Graphic Design</strong></h4>
                    My self driven flair for design and eye for detail
                    allows me to produce the most memorable designs. 
                    I am Capable of creating a brand from scratch, 
                    or working creatively within the aesthetics 
                    of my client’s designated brand standards.
                </div>
                
                <div class="col-md-4">
                    <img src="assets/img/services/cogs.png" alt="technician"/>
                    <h4><strong>Network | Software | Hardware</strong></h4>
                    I am a competent Network, Software and Hardware Technician.
                    Wether it be building a computer from scratch to building an
                    entire network, I can set up domains, servers, VLANS, 
                    switches &amp; routers.
                    Centos, Ubuntu, Redhat or windows, I'm your guy!  
                </div>
                
            
            </div>
        </div>


    </section>
    <!-- SERVICES SECTION END  -->
    
    <section id="works">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-6 col-md-offset-3 pad-bottom" data-scroll-reveal="enter from the bottom after 0.2s">
                    <h2><strong>WORK PORFOLIO</strong></h2>
                </div>
            </div>

            <div class="row text-center">
                <div class="col-md-12 pad-bottom" data-scroll-reveal="enter from the bottom after 0.4s">
                    <div class="caegories">
                        <a href="#" data-filter="*" class="active btn btn-info btn-sm">All</a>
                        <a href="#" data-filter=".html" class="btn btn-info btn-sm">HTML/CSS</a>
                        <a href="#" data-filter=".css" class="btn btn-info btn-sm">VARIOUS TECHNOLOGIES</a>
                        <a href="#" data-filter=".code" class="btn btn-info btn-sm">FRAMEWORKS</a>
                        <a href="#" data-filter=".script" class="btn btn-info btn-sm">GRAPHIC DESIGN</a>
                    </div>

                </div>
            </div>


            <div class="row text-center" id="portfolio-div" >

                <div class="col-md-4 col-sm-4 html">
                    <div class="work-wrapper">

                        <a class="fancybox-media" title="<strong><span class='fancyZoomLinks'><br />This website was my 
                        first attempt at developing, I did this during my first year at university. Although not the best looking, this site set the
                        foundations to my web development skills<br /><br /></span></strong>
                        <span class='fancyZoomButtons'><a href='firstPort.php' class='btn-readmore btn-default set-bk-clr'>READ MORE</a></span><br />
                        <span class='fancyZoomLinks'><a target='_blank' href='../saffronspice'>Click here to view site</a></span>" href="assets/img/portfolio/saffronSpice.png">

                            <img src="assets/img/portfolio/saffronSpiceShow.png" class="img-responsive " alt="Interactive Internet Systems" />
                        </a>


                    </div>
                </div>
                <div class="col-md-4 col-sm-4 html">
                    <div class="work-wrapper">
                        <a class="fancybox-media" title="<span class='fancyZoomLinks'><strong><br />Silver Ice a 
                        Ecommerce Media website, was designed and created for the purpose of a University web project. 
                        PHP, HTML, and JavaScript languages were utilised. A customised 960 grid system was used for the front end development
                        to create a efficient, realistic, and proffessional end product.<br /><br /></span></strong>
                        
                        <span class='fancyZoomButtons'><a href='secondPort.php' class='btn-readmore btn-default set-bk-clr'>READ MORE</a></span><br />
                        <span class='fancyZoomLinks'><a target='_blank' href='../silverice'>Click Here to visit the Site</a></span>" href="assets/img/portfolio/silverIce.png">

                            <img src="assets/img/portfolio/silverIceShow.png" class="img-responsive " alt="Silver Ice Logo" />
                        </a>

                    </div>
                </div>
                <div class="col-md-4 col-sm-4 html css code">
                    <div class="work-wrapper">
                        <a class="fancybox-media" title="<span class='fancyZoomLinks'><strong><br />AIDA 
                        was part of a core module to develop a website utilising various technologies/languages to
                        show a clear understanding of how to use them.<br /><br /></span></strong>
                        
                        <span class='fancyZoomButtons'><a href='thirdPort.php' class='btn-readmore btn-default set-bk-clr'>READ MORE</a></span><br />
                        <span class='fancyZoomLinks'><a target='_blank' href='../AIDC3337661'>Click Here to visit the Site</a></span>" href="assets/img/portfolio/aidaShow.png">

                            <img src="assets/img/portfolio/aida.png" class="img-responsive " alt="" />
                        </a>

                    </div>
                </div>
                <div class="col-md-4 col-sm-4 code html">
                    <div class="work-wrapper">
                        <a class="fancybox-media" title="<span class='fancyZoomLinks'><strong><br />Smart Agent 
                        is still under construction. SmartAgent is part of my final year product.<br /><br /></span></strong>
                        
                        <span class='fancyZoomButtons'><a href='fourthPort.php' class='btn-readmore btn-default set-bk-clr'>READ MORE</a></span><br />
                        <span class='fancyZoomLinks'><a target='_blank' href='../PP'>Click Here to visit the Site</a></span>" href="assets/img/portfolio/smartAgent.png">

                            <img src="assets/img/portfolio/smartAgentShow.png" class="img-responsive " alt="" />
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 script">
                    <div class="work-wrapper">
                        <a class="fancybox-media" title="<span class='fancyZoomLinks'><strong><br />This was a graphic design project wherein I had designed
                        a menu for a Indian &amp; fastfood takeaway, I had also designed thier logo for them.<br /><br/>
                        <a target='_blank' href='pdf/pbh.pdf'>Click here to view the full menu.</a></span></strong>" href="assets/img/portfolio/pbh.png">

                            <img src="assets/img/portfolio/pbhShow.png" class="img-responsive " alt="" />
                        </a>

                    </div>
                </div>
                <div class="col-md-4 col-sm-4 script">
                    <div class="work-wrapper">
                        <a class="fancybox-media" title="<span class='fancyZoomLinks'><strong><br />Here is Graphic Design Project which consisted of making a 
                        static logo as well as a animated logo for a YouTuber. This project allowed me to enhance and
                        utilies my Adobe PhotoShop and Adobe After Effects skills.<br /><br /> 
                        <a target='_blank' data-type='ogv' href='assets/video/bbr.ogv'>Click here to See Animated Video</a></span></strong>" href="assets/img/portfolio/bbRoxc2.png">

                            <img src="assets/img/portfolio/bbRoxcShow.png" class="img-responsive" alt="BeautyByRoxc Logo" />
                            
                        </a>

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- WORK SECTION END  -->

    <section id="about">
        <div class="container">

            <div class="row text-center" data-scroll-reveal="enter from the bottom after 0.2s">
                <div class="col-md-6 col-md-offset-3 pad-bottom">
                    <h2><strong>ABOUT ME</strong></h2>
                </div>
            </div>

            <div class="row pad-bottom" data-scroll-reveal="enter from the bottom after 0.4s">
                
                <div class="col-md-6">

                    <span class="portImage"><img src="assets/img/me.png" alt="me" class="img-circle" /></span>
                            <br />
                            <span class="fancyZoomButtons">
                            My name is Abdush I am a 22 year old developer designer and technician.
                            I take extra care in what I do and believe practice makes perfect.

                            I have a few project's I have been working on, please see my portfolio for
                            more information and an overview of what I am capable of doing.
                            </span>
                            <br />
                </div>

                <div class="col-md-3">
                    <div class="panel panel-heading">
                        My Skills Include:
                    </div> 
                    <div class="panel body">
                        <ul>
                            <li>HTML</li>
                            <li>CSS</li>
                            <li>PHP</li>
                            <li>Frameworks | Front End | Back End</li>
                            <li>jQuery</li>
                            <li>javaScript</li>
                            <li>ajax</li>
                            <li>Servers</li>
                            <li>Hardware / Software</li>
                            <li>Adobe Creative Suite</li>
                        </ul>
                    </div>   
                </div>

                <div class="col-md-3">
                    <div class="panel panel-heading">
                        Curriculum Vitae Overview:
                    </div> 
                    <div class="panel body">
                        <ul>
                            <li>BSc Computing Undergraduate</li>
                            <br />
                            <li>16 GCSE's Graded A-C</li>
                            <br />
                            <li>A Levels Graded AAA</li>
                            <br />
                            <li>2 Years ICT 1st Line / 2nd Line Support Experience</li>
                            <br />
                            <li>Competent Programmer / Designer / Developer</li>
                        </ul>
                    </div>   
                </div>

            </div>
            <span class="dloadCv">
            <a data-scroll-reveal="enter from the bottom after 0.6s" target="_blank" href="pdf/cv.pdf" class="btn-xlarge btn-default set-bk-clr animated">View / Download Full CV</a>
            </span>
        </div>

    </section>
    <!-- TEAM SECTION END -->

    <section id="contact">
        <div class="overlay">
            <div class="container">

            <div class="row">
                <div class="col-lg-12 text-center">

                    <h2 class="section-heading">Drop me a line...</h2>
                    
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <br />
                  <!-- Form itself -->
                  <form name="sentMessage" id="contactForm"  novalidate>
                     <div class="control-group">
                        <div class="controls">
                        <input type="text" class="form-control" 
                               placeholder="Full Name" id="name" required
                                   data-validation-required-message="Please enter your name" />
                          <p class="help-block"></p>
                        </div>
                    </div>     
                    <div class="control-group">
                        <div class="controls">
                        <input type="email" class="form-control" placeholder="Email" 
                                        id="email" required
                                   data-validation-required-message="Please enter your email" />
                        </div>
                    </div>  
                      
                    <div class="control-group">
                        <div class="controls">
                         <textarea rows="10" cols="100" class="form-control" 
                               placeholder="Message" id="message" required
                       data-validation-required-message="Please enter your message" minlength="5" 
                               data-validation-minlength-message="Min 5 characters" 
                                maxlength="999" style="resize:none"></textarea>
                        </div>
                    </div>        
                    <div id="success"> </div> <!-- For success/fail messages -->
                    <br />
                    <span class="fancyZoomButtons"><button type="submit" class="btn-xlarge btn-default set-bk-clr">Send</button></span><br />
                </form>

            </div>
        </div>



       </div>
    </div>
</section>
    <!-- CONTACT SECTION END -->
    <!-- http://myprogrammingblog.com/2013/08/27/how-to-make-a-contact-form-with-bootstrap-3-jqueryphphtml5jqbootstrapvalidation/ -->