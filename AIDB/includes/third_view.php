<div class="navbar navbar-default navbar-fixed-top scroll-me">
        <!-- pass scroll-me class above a tags to starts scrolling -->
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <!--<img src="assets/img/logo.png"  alt=""/>-->
                </a>
            </div>
            <div class="navbar-collapse collapse animated bounceInDown">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php#works">GO BACK TO PORTFOLIO</a></li>
                </ul>
            </div>

        </div>
    </div>
    <!-- NAVBAR END  -->
    <!--HEADER SECTION END  -->
    <section id="services">
    <div class="container">
            <div class="row text-center">
                <div class="col-md-6 col-md-offset-3 pad-bottom" data-scroll-reveal="enter from the bottom after 0.2s">
                    <h2><strong>PORTFOLIO | AIDA</strong></h2>
                </div>
            </div>
    <div class="row">
        <div class="col-md-4" data-scroll-reveal="enter from the bottom after 0.4s">
            <h4><strong>AIDA</strong></h4>
                <p>This website was created within my final year at university.
                as part of a core module. This site helped me further develop 
                coding skills, with introduction to various new languages outlined
                below. This site also introduced best coding practises, further security,
                efficiency, integration of third party services and moreover new approaches.</p>

                <p>Above all, one of the main new approaches learned was the use
                and integration of framworks, and the importance of it. This project
                itself uses the front end framework: Bootstrap in addition to all other
                technologies and approaches used.</p>
        </div> <!-- /col-md-4 -->
        
        <div class="col-md-8" data-scroll-reveal="enter from the bottom after 0.4s">
            <!-- begin macbook pro mockup -->
            <div class="md-macbook-pro md-glare">
                <div class="md-lid">
                    <div class="md-camera"></div>
                    <div class="md-screen">
                    <!-- content goes here -->                
                        <div class="tab-featured-image">
                            <div class="tab-content">
                                <div class="tab-pane  in active" id="tab1">
                                    <img src="assets/img/portfolio/aidaWeb.png">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="md-base"></div>
            </div> <!-- end macbook pro mockup -->

            <span class='fancyZoomLinks'><strong><a target='_blank' href='../AIDC3337661'>Click here to view site</a></strong></span>

        </div> <!-- / .col-md-8 -->

    </div> <!--/ .row -->
</div> <!-- end sidetab container -->
<div class="container">
    <div class="row text-center">
        <div class="col-md-6 col-md-offset-3 pad-bottom" data-scroll-reveal="enter from the bottom after 0.2s">
            <h4><strong>TECHNOLOGIES USED</strong></h4>
        </div>
    </div>
    <div class="row" data-scroll-reveal="enter from the bottom after 0.6s">                   
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width: 35%;">
                <span class="sr-only">35% Complete</span>
            </div>
            <span class="progress-type">HTML / HTML5</span>
            <span class="progress-completed">35%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                <span class="sr-only">100% Complete (danger)</span>
            </div>
            <span class="progress-type">CSS / CSS3 / FRONT END FRAMEWORKS</span>
            <span class="progress-completed">100%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                <span class="sr-only">65% Complete (success)</span>
            </div>
            <span class="progress-type">PHP</span>
            <span class="progress-completed">65%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                <span class="sr-only">50% Complete (info)</span>
            </div>
            <span class="progress-type">XML</span>
            <span class="progress-completed">50%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%">
                <span class="sr-only">25% Complete (success)</span>
            </div>
            <span class="progress-type">Json</span>
            <span class="progress-completed">25%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                <span class="sr-only">15% Complete (warning)</span>
            </div>
            <span class="progress-type">JavaScript / jQuery</span>
            <span class="progress-completed">15%</span>
        </div>
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
                <span class="sr-only">20% Complete</span>
            </div>
            <span class="progress-type">Ajax</span>
            <span class="progress-completed">20%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                <span class="sr-only">10% Complete (danger)</span>
            </div>
            <span class="progress-type">REST</span>
            <span class="progress-completed">10%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                <span class="sr-only">70% Complete (info)</span>
            </div>
            <span class="progress-type">OO PHP PDO</span>
            <span class="progress-completed">70%</span>
        </div>
    </div>
</div>

</section>