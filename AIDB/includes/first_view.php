
<div class="navbar navbar-default navbar-fixed-top scroll-me">
        <!-- pass scroll-me class above a tags to starts scrolling -->
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <!--<img src="assets/img/logo.png"  alt=""/>-->
                </a>
            </div>
            <div class="navbar-collapse collapse animated bounceInDown">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php#works">GO BACK TO PORTFOLIO</a></li>
                </ul>
            </div>

        </div>
    </div>
    <!-- NAVBAR END  -->
    <!--HEADER SECTION END  -->
    <section id="services">
    <div class="container">
            <div class="row text-center">
                <div class="col-md-6 col-md-offset-3 pad-bottom" data-scroll-reveal="enter from the bottom after 0.2s">
                    <h2><strong>PORTFOLIO | SAFFRON SPICE</strong></h2>
                </div>
            </div>
    <div class="row">
        <div class="col-md-4" data-scroll-reveal="enter from the bottom after 0.4s">
            <h4><strong>SAFFRON TANDOORI SPICE</strong></h4>
                <p>This website was created within my first year at university.
                This site laid down the foundations to my css and html skills.
                This website is an example of a simple html and CSS website using 
                a grid system.</p>

                <p>As this was my first attempt at coding, this website is semi responsive.
                With this being a learning curve the site is reasonably okay, 
                obviously there is room for many improvements to be made.</p>

                <p>I was also involved in a real life graphic design project for this 
                company and have designed a logo and Menu.</p>

                <p><strong><a target="_blank" href="pdf/sspice.pdf">Click here to view the menu.</a></strong></p>
             
        </div> <!-- /col-md-4 -->
        
        <div class="col-md-8" data-scroll-reveal="enter from the bottom after 0.4s">
            <!-- begin macbook pro mockup -->
            <div class="md-macbook-pro md-glare">
                <div class="md-lid">
                    <div class="md-camera"></div>
                    <div class="md-screen">
                    <!-- content goes here -->                
                        <div class="tab-featured-image">
                            <div class="tab-content">
                                <div class="tab-pane  in active" id="tab1">
                                    <img src="assets/img/portfolio/saffronSpiceWeb.png">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="md-base"></div>
            </div> <!-- end macbook pro mockup -->

            <span class='fancyZoomLinks'><strong><a target='_blank' href='../saffronspice'>Click here to view site</a></strong></span>

        </div> <!-- / .col-md-8 -->

    </div> <!--/ .row -->
</div> <!-- end sidetab container -->
<div class="container">
    <div class="row text-center">
        <div class="col-md-6 col-md-offset-3 pad-bottom" data-scroll-reveal="enter from the bottom after 0.2s">
            <h4><strong>TECHNOLOGIES USED</strong></h4>
        </div>
    </div>
    <div class="row" data-scroll-reveal="enter from the bottom after 0.6s">                   
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                <span class="sr-only">100% Complete</span>
            </div>
            <span class="progress-type">HTML / HTML5</span>
            <span class="progress-completed">100%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                <span class="sr-only">100% Complete (danger)</span>
            </div>
            <span class="progress-type">CSS / CSS3</span>
            <span class="progress-completed">100%</span>
        </div>
        
    </div>
</div>

</section>