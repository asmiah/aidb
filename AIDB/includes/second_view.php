
<div class="navbar navbar-default navbar-fixed-top scroll-me">
        <!-- pass scroll-me class above a tags to starts scrolling -->
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <!--<img src="assets/img/logo.png"  alt=""/>-->
                </a>
            </div>
            <div class="navbar-collapse collapse animated bounceInDown">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php#works">GO BACK TO PORTFOLIO</a></li>
                </ul>
            </div>

        </div>
    </div>
    <!-- NAVBAR END  -->
    <!--HEADER SECTION END  -->
    <section id="services">
    <div class="container">
            <div class="row text-center">
                <div class="col-md-6 col-md-offset-3 pad-bottom" data-scroll-reveal="enter from the bottom after 0.2s">
                    <h2><strong>PORTFOLIO | SILVER ICE</strong></h2>
                </div>
            </div>
    <div class="row">
        <div class="col-md-4" data-scroll-reveal="enter from the bottom after 0.4s">
            <h4><strong>SILVER ICE</strong></h4>
                <p>This website was created within my second year at university.
                This site helped me further develop my HTML CSS skills, as well as
                introduced the use of new languages. These languages involved PHP
                JavaScript and SQL.</p>

                <p>This website is an ecommerce website, designed to mimick a live
                media site, with games and movies for sale. The site was built as part
                of a team project. My role within the project was to design the 
                security and validation features. This introduced me to client side
                server side validation, as well as sessions, login, register systems,
                and much more.</p>
             
        </div> <!-- /col-md-4 -->
        
        <div class="col-md-8" data-scroll-reveal="enter from the bottom after 0.4s">
            <!-- begin macbook pro mockup -->
            <div class="md-macbook-pro md-glare">
                <div class="md-lid">
                    <div class="md-camera"></div>
                    <div class="md-screen">
                    <!-- content goes here -->                
                        <div class="tab-featured-image">
                            <div class="tab-content">
                                <div class="tab-pane  in active" id="tab1">
                                    <img src="assets/img/portfolio/silverIceWeb.png">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="md-base"></div>
            </div> <!-- end macbook pro mockup -->

            <span class='fancyZoomLinks'><strong><a target='_blank' href='../silverice'>Click here to view site</a></strong></span>

        </div> <!-- / .col-md-8 -->

    </div> <!--/ .row -->
</div> <!-- end sidetab container -->
<div class="container">
    <div class="row text-center">
        <div class="col-md-6 col-md-offset-3 pad-bottom" data-scroll-reveal="enter from the bottom after 0.2s">
            <h4><strong>TECHNOLOGIES USED</strong></h4>
        </div>
    </div>
    <div class="row" data-scroll-reveal="enter from the bottom after 0.6s">                   
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width: 35%;">
                <span class="sr-only">35% Complete</span>
            </div>
            <span class="progress-type">HTML / HTML5</span>
            <span class="progress-completed">35%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                <span class="sr-only">100% Complete (danger)</span>
            </div>
            <span class="progress-type">CSS / CSS3</span>
            <span class="progress-completed">100%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                <span class="sr-only">65% Complete (success)</span>
            </div>
            <span class="progress-type">PHP</span>
            <span class="progress-completed">65%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                <span class="sr-only">50% Complete (info)</span>
            </div>
            <span class="progress-type">SQL</span>
            <span class="progress-completed">50%</span>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                <span class="sr-only">15% Complete (warning)</span>
            </div>
            <span class="progress-type">JavaScript / jQuery</span>
            <span class="progress-completed">15%</span>
        </div>
    </div>
</div>

</section>